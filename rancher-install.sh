#!/usr/bin/env bash
echo "停止现有rancher容器"
docker stop rancher

echo "删除现有rancher容器"
docker rm rancher

echo "删除rancher外置数据文件夹"
rm -rf /var/lib/rancher/

echo "创建rancher外置数据文件夹"
mkdir -p /var/lib/rancher/

echo "拉取rancher镜像"
docker pull rancher/rancher:stable

echo "运行rancher"
docker run -d --name rancher --restart=unless-stopped \
-p 80:80 -p 443:443 \
-v /var/lib/rancher/:/var/lib/rancher/ \
-v /root/var/log/auditlog:/var/log/auditlog \
-e AUDIT_LEVEL=3 \
rancher/rancher:stable

echo "查看日志"
docker logs -f rancher